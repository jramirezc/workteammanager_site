﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class SkillConfiguration : IEntity, IAuditable, IDeletable
    {
        public SkillConfiguration()
        {
        }

        public SkillConfiguration(int value, int workerId, int skillId)
        {
            Value = value;
            WorkerId = workerId;
            SkillId = skillId;
        }
        
        public int Value { get; set; }
        public int WorkerId { get; set; }
        public int SkillId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}