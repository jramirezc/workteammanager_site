﻿using System;
using WorkTeamManager.Domain.Enums;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Proyect : IEntity, IAuditable, IDeletable
    {
        public Proyect()
        {
        }

        public Proyect(Tracing tracing, string code, string name, string customer, string repositoryRoot, int businessUnitId)
        {
            Tracing = tracing;
            Code = code;
            Name = name;
            Customer = customer;
            RepositoryRoot = repositoryRoot;
            BusinessUnitId = businessUnitId;
        }
        Tracing Tracing { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Customer { get; set; }
        public string RepositoryRoot { get; set; }
        public int BusinessUnitId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}