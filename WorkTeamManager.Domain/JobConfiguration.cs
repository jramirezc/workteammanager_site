﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class JobConfiguration : IEntity, IAuditable, IDeletable
    {
        public JobConfiguration()
        {
        }

        public JobConfiguration(string code, decimal salary, int jobId, int? categoryId, int? levelId)
        {
            Code = code;
            Salary = salary;
            JobId = jobId;
            CategoryId = categoryId;
            LevelId = levelId;
        }
        
        public string Code { get; set; }

        public decimal Salary { get; set; }

        public int JobId { get; set; }

        public int? CategoryId { get; set; }

        public int? LevelId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}