﻿namespace WorkTeamManager.Domain.Interfaces
{
    public interface IDeletable
    {
         bool IsActive { get; set; }
    }
}