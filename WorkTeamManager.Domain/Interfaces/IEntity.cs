namespace WorkTeamManager.Domain.Interfaces
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}