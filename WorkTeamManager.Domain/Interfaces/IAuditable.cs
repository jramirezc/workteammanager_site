using System;

namespace WorkTeamManager.Domain.Interfaces
{
    public interface IAuditable
    {
        string CreatedBy { get; set; }
        DateTimeOffset CreatedOn { get; set; }
        string ModifiedBy { get; set; }
        DateTimeOffset ModifiedOn { get; set; }
        bool Status { get; set; }
    }
}