﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class EnviromentConfiguration : IEntity, IAuditable, IDeletable
    {
        public EnviromentConfiguration()
        {
        }

        public EnviromentConfiguration(string key, string value, int enviromentId)
        {
            Key = key;
            Value = value;
            EnviromentId = enviromentId;
        }
        
        public string Key { get; set; }
        public string Value { get; set; }
        public int EnviromentId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}