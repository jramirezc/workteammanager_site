﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class ProyectConfiguration : IEntity, IAuditable, IDeletable
    {
        public ProyectConfiguration()
        {
        }

        public ProyectConfiguration(int proyectId, int workerId, int? tagId)
        {
            ProyectId = proyectId;
            WorkerId = workerId;
            TagId = tagId;
        }
        
        public int ProyectId { get; set; }
        public int WorkerId { get; set; }
        public int? TagId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}