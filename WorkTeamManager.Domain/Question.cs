﻿using System;
using WorkTeamManager.Domain.Enums;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Question : IEntity, IAuditable, IDeletable
    {
        public Question()
        {
        }

        public Question(QuestionType questionType, int order, string name, decimal? value, bool required, int? lowerAnswerRequired, int? topAnswerRequired, int surveyId)
        {
            QuestionType = questionType;
            Order = order;
            Name = name;
            Value = value;
            Required = required;
            LowerAnswerRequired = lowerAnswerRequired;
            TopAnswerRequired = topAnswerRequired;
            SurveyId = surveyId;
        }

        public QuestionType QuestionType { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public decimal? Value { get; set; }
        public bool Required { get; set; }
        public int? LowerAnswerRequired { get; set; }
        public int? TopAnswerRequired { get; set; }
        public int SurveyId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}