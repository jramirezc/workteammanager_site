﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Job : IEntity, IAuditable, IDeletable
    {
        public Job()
        {
        }

        public Job(string name)
        {
            Name = name;
        }
        
        public string Name { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}