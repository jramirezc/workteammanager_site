﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Answer : IEntity, IAuditable, IDeletable
    {
        public Answer()
        {
        }

        public Answer(string result, int questionId, int questionOptionId, int surveyResultId)
        {
            Result = result;
            QuestionId = questionId;
            QuestionOptionId = questionOptionId;
            SurveyResultId = surveyResultId;
        }
        
        public string Result { get; set; }
        public int QuestionId { get; set; }
        public int QuestionOptionId { get; set; }
        public int SurveyResultId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}