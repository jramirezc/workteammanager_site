﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class RefreshToken : IEntity, IAuditable, IDeletable
    {
        public RefreshToken()
        {
        }

        public RefreshToken(string ticket, int minutesLifeTime, int workerId)
        {
            Ticket = ticket;
            IssuedDate = DateTimeOffset.UtcNow;
            ExpirationDate = IssuedDate.AddMinutes(minutesLifeTime);
            WorkerId = workerId;
        }

        public string Ticket { get; set; }
        public DateTimeOffset IssuedDate { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
        public int WorkerId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}