﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class BusinessUnit : IEntity, IAuditable, IDeletable
    {
        public BusinessUnit()
        {
        }

        public BusinessUnit(string code, string name, string address)
        {
            Code = code;
            Name = name;
            Address = address;
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}