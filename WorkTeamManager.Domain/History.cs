﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class History : IEntity
    {
        public History()
        {
        }

        public History(string name, int proyectId)
        {
            Name = name;
            DateEvent = DateTimeOffset.UtcNow;
            ProyectId = proyectId;
        }
        
        public string Name { get; set; }
        public DateTimeOffset DateEvent { get; set; }
        public int ProyectId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion
    }
}