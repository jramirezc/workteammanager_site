﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class SurveyResult : IEntity, IAuditable, IDeletable
    {
        public SurveyResult()
        {
        }

        public SurveyResult(string comment, int surveyId, int appliedBy, int? applyTo)
        {
            Comment = comment;
            SurveyId = surveyId;
            AppliedBy = appliedBy;
            ApplyTo = applyTo;
        }
        
        public string Comment { get; set; }
        public int SurveyId { get; set; }
        public int AppliedBy { get; set; }
        public int? ApplyTo { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}