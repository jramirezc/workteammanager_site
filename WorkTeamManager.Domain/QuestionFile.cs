﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class QuestionFile : IEntity, IAuditable, IDeletable
    {
        public QuestionFile()
        {
        }

        public QuestionFile(string name, string filePath, int questionId)
        {
            Name = name;
            FilePath = filePath;
            QuestionId = questionId;
        }
        
        public string Name { get; set; }
        public string FilePath { get; set; }
        public int QuestionId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}