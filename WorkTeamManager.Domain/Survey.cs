﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Survey : IEntity, IAuditable, IDeletable
    {
        public Survey()
        {
        }

        public Survey(string name, string description, decimal? value, bool weighted)
        {
            Name = name;
            Description = description;
            Value = value;
            Weighted = weighted;
        }
        
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Value { get; set; }
        public bool Weighted { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}