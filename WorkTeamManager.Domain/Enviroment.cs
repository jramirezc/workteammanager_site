﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Enviroment : IEntity, IAuditable, IDeletable
    {
        public Enviroment()
        {
        }

        public Enviroment(string name, int proyectId)
        {
            Name = name;
            ProyectId = proyectId;
        }
        
        public string Name { get; set; }
        public int ProyectId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}