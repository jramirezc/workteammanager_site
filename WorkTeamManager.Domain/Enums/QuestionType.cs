﻿namespace WorkTeamManager.Domain.Enums
{
    public enum QuestionType
    {
        Text = 1,
        Integer = 2,
        SingleChoice = 3,
        MultipleChoice = 4,
        Dichotomy = 5
    }
}