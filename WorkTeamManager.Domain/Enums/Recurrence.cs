﻿namespace WorkTeamManager.Domain.Enums
{
    public enum Recurrence
    {
        Weekly = 1,
        Monthly = 2
    }
}