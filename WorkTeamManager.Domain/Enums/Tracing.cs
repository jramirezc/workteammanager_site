﻿namespace WorkTeamManager.Domain.Enums
{
    public enum Tracing
    {
        Pending = 1,
        InProcess = 2,
        Paused = 3,
        Finalized = 4,
        Cancelled = 5
    }
}