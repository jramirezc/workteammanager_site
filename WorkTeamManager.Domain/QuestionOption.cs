﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class QuestionOption : IEntity, IAuditable, IDeletable
    {
        public QuestionOption()
        {
        }

        public QuestionOption(string text, double? value, int questionId)
        {
            Text = text;
            Value = value;
            QuestionId = questionId;
        }
        
        public string Text { get; set; }
        public double? Value { get; set; }
        public int QuestionId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}