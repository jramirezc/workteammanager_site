﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Career : IEntity
    {
        public Career()
        {
        }

        public Career(string name, int workerId)
        {
            Name = name;
            DateEvent = DateTimeOffset.UtcNow;
            WorkerId = workerId;
        }

        public string Name { get; set; }
        public DateTimeOffset DateEvent { get; set; }
        public int WorkerId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion
    }
}