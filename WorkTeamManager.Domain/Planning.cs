﻿using System;
using WorkTeamManager.Domain.Enums;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Planning : IEntity
    {
        public Planning()
        {
        }

        public Planning(Recurrence recurrence, string name, string description, DateTimeOffset initialDate, DateTimeOffset expirationDate, string days)
        {
            Recurrence = recurrence;
            Name = name;
            Description = description;
            InitialDate = initialDate;
            ExpirationDate = expirationDate;
            Days = days;
        }

        public Recurrence Recurrence { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset InitialDate { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
        public string Days { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion
    }
}