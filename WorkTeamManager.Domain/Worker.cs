﻿using System;
using WorkTeamManager.Domain.Interfaces;

namespace WorkTeamManager.Domain
{
    public class Worker : IEntity, IAuditable, IDeletable
    {
        public Worker()
        {
        }

        public Worker(string name, bool isAdmin, string email, string password, int? businessUnitId, int? jobConfigurationId)
        {
            Name = name;
            IsAdmin = isAdmin;
            Email = email;
            Password = password;
            BusinessUnitId = businessUnitId;
            JobConfigurationId = jobConfigurationId;
        }

        public string Name { get; set; }
        public bool IsAdmin { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int? BusinessUnitId { get; set; }
        public int? JobConfigurationId { get; set; }

        #region IEntity Members

        public string Id { get; set; }

        #endregion

        #region IAuditable Members

        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public bool Status { get; set; }

        #endregion

        #region IDeletable Members

        public bool IsActive { get; set; }

        #endregion
    }
}