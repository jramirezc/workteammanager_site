﻿using FluentMigrator;

namespace WorkTeamManager.Migration.Sprint_01
{
    [Migration(1)]
    public class _1_CoreMigration : FluentMigrator.Migration
    {
        
        public override void Up()
        {

            #region Job

            Create.Table("Job").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

            #region Category

            Create.Table("Category").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

            #region Level

            Create.Table("Level").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

            #region JobConfiguration

            Create.Table("JobConfiguration").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Code").AsString(250).NotNullable()
                .WithColumn("Salary").AsDecimal(10, 2).Nullable()
                .WithColumn("JobId").AsInt32().NotNullable()
                .WithColumn("CategoryId").AsInt32().Nullable()
                .WithColumn("LevelId").AsInt32().Nullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_JobConfiguration_Job").FromTable("JobConfiguration").InSchema("dbo").ForeignColumn("JobId")
                .ToTable("Job").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_JobConfiguration_Category").FromTable("JobConfiguration").InSchema("dbo").ForeignColumn("CategoryId")
                .ToTable("Category").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_JobConfiguration_Level").FromTable("JobConfiguration").InSchema("dbo").ForeignColumn("LevelId")
                .ToTable("Level").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_JobConfiguration").OnTable("JobConfiguration").InSchema("dbo").OnColumn("JobId").Ascending()
                .OnColumn("CategoryId").Ascending().OnColumn("LevelId").Ascending().WithOptions().Unique();

            #endregion

            #region BusinessUnit

            Create.Table("BusinessUnit").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Code").AsString(50).NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("Address").AsString(250)
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

            #region Worker

            Create.Table("Worker").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("IsAdmin").AsBoolean().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("Email").AsString(250).NotNullable()
                .WithColumn("Password").AsString(250).NotNullable()
                .WithColumn("BusinessUnitId").AsInt32().Nullable()
                .WithColumn("JobConfigurationId").AsInt32().Nullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_Worker_BusinessUnit").FromTable("Worker").InSchema("dbo").ForeignColumn("BusinessUnitId")
                .ToTable("BusinessUnit").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_Worker_JobConfiguration").FromTable("Worker").InSchema("dbo").ForeignColumn("JobConfigurationId")
                .ToTable("JobConfiguration").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_Worker").OnTable("Worker").InSchema("dbo").OnColumn("BusinessUnitId").Ascending()
                .OnColumn("JobConfigurationId").Ascending().WithOptions().Unique();

            #endregion

            #region RefreshToken

            Create.Table("RefreshToken").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Ticket").AsString(250).NotNullable()
                .WithColumn("IssuedDate").AsDateTimeOffset().NotNullable()
                .WithColumn("ExpirationDate").AsDateTimeOffset().NotNullable()
                .WithColumn("WorkerId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_RefreshToken_Worker").FromTable("RefreshToken").InSchema("dbo").ForeignColumn("WorkerId")
                .ToTable("Worker").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_RefreshToken").OnTable("RefreshToken").InSchema("dbo").OnColumn("WorkerId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region Career

            Create.Table("Career").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("WorkerId").AsInt32().NotNullable()
                .WithColumn("DateEvent").AsDateTimeOffset().NotNullable();

            Create.ForeignKey("FK_Career_Worker").FromTable("Career").InSchema("dbo").ForeignColumn("WorkerId")
                .ToTable("Worker").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_Career").OnTable("Career").InSchema("dbo").OnColumn("WorkerId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region Skill

            Create.Table("Skill").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

            #region SkillConfiguration

            Create.Table("SkillConfiguration").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Value").AsInt32().NotNullable()
                .WithColumn("WorkerId").AsInt32().NotNullable()
                .WithColumn("SkillId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_SkillConfiguration_Worker").FromTable("SkillConfiguration").InSchema("dbo").ForeignColumn("WorkerId")
                .ToTable("Worker").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_SkillConfiguration_Skill").FromTable("SkillConfiguration").InSchema("dbo").ForeignColumn("SkillId")
                .ToTable("Skill").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_SkillConfiguration").OnTable("SkillConfiguration").InSchema("dbo").OnColumn("WorkerId").Ascending()
                .OnColumn("SkillId").Ascending().WithOptions().Unique();

            #endregion

            #region Proyect

            Create.Table("Proyect").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Tracing").AsInt32().NotNullable()
                .WithColumn("Code").AsString(250).Nullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("Customer").AsString(250).NotNullable()
                .WithColumn("RepositoryRoot").AsString(250).Nullable()
                .WithColumn("BusinessUnitId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_Proyect_BusinessUnit").FromTable("Proyect").InSchema("dbo").ForeignColumn("BusinessUnitId")
                .ToTable("BusinessUnit").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_Proyect").OnTable("Proyect").InSchema("dbo").OnColumn("BusinessUnitId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region Enviroment

            Create.Table("Enviroment").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("ProyectId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_Enviroment_Proyect").FromTable("Enviroment").InSchema("dbo").ForeignColumn("ProyectId")
                .ToTable("Proyect").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_Enviroment").OnTable("Enviroment").InSchema("dbo").OnColumn("ProyectId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region EnviromentConfiguration

            Create.Table("EnviromentConfiguration").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Key").AsString(250).NotNullable()
                .WithColumn("Value").AsString(250).NotNullable()
                .WithColumn("EnviromentId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_EnviromentConfiguration_Enviroment").FromTable("EnviromentConfiguration").InSchema("dbo").ForeignColumn("EnviromentId")
                .ToTable("Enviroment").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_EnviromentConfiguration").OnTable("EnviromentConfiguration").InSchema("dbo").OnColumn("EnviromentId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region History

            Create.Table("History").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("ProyectId").AsInt32().NotNullable()
                .WithColumn("DateEvent").AsDateTimeOffset().NotNullable();

            Create.ForeignKey("FK_History_Proyect").FromTable("History").InSchema("dbo").ForeignColumn("ProyectId")
                .ToTable("Proyect").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_History").OnTable("History").InSchema("dbo").OnColumn("ProyectId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region Tag

            Create.Table("Tag").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Code").AsString(250).NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("BackgroundColor").AsString(250).NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

            #region ProyectConfiguration

            Create.Table("ProyectConfiguration").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("ProyectId").AsInt32().NotNullable()
                .WithColumn("WorkerId").AsInt32().NotNullable()
                .WithColumn("TagId").AsInt32().Nullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_ProyectConfiguration_Proyect").FromTable("ProyectConfiguration").InSchema("dbo").ForeignColumn("ProyectId")
                .ToTable("Proyect").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_ProyectConfiguration_Worker").FromTable("ProyectConfiguration").InSchema("dbo").ForeignColumn("WorkerId")
                .ToTable("Worker").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_ProyectConfiguration_Tag").FromTable("ProyectConfiguration").InSchema("dbo").ForeignColumn("TagId")
                .ToTable("Tag").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_ProyectConfiguration").OnTable("ProyectConfiguration").InSchema("dbo").OnColumn("ProyectId").Ascending()
                .OnColumn("WorkerId").Ascending().OnColumn("TagId").Ascending().WithOptions().Unique();

            #endregion

            #region Survey

            Create.Table("Survey").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("Description").AsString(250).Nullable()
                .WithColumn("Value").AsDecimal(10,2).Nullable()
                .WithColumn("Weighted").AsBoolean().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

            #region Question

            Create.Table("Question").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("QuestionType").AsInt32().NotNullable()
                .WithColumn("Order").AsInt32().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("Value").AsDecimal(10,2).Nullable()
                .WithColumn("Required").AsBoolean().Nullable()
                .WithColumn("LowerAnswerRequired").AsInt32().Nullable()
                .WithColumn("TopAnswerRequired").AsInt32().Nullable()
                .WithColumn("SurveyId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_Question_Survey").FromTable("Question").InSchema("dbo").ForeignColumn("SurveyId")
                .ToTable("Survey").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_Question").OnTable("Question").InSchema("dbo").OnColumn("SurveyId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region QuestionOption

            Create.Table("QuestionOption").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Text").AsString(250).NotNullable()
                .WithColumn("Value").AsDecimal(10,2).Nullable()
                .WithColumn("QuestionId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_QuestionOption_Question").FromTable("QuestionOption").InSchema("dbo").ForeignColumn("QuestionId")
                .ToTable("Question").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_QuestionOption").OnTable("QuestionOption").InSchema("dbo").OnColumn("QuestionId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region QuestionFile

            Create.Table("QuestionFile").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("FilePath").AsString(250).NotNullable()
                .WithColumn("QuestionId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_QuestionFile_Question").FromTable("QuestionFile").InSchema("dbo").ForeignColumn("QuestionId")
                .ToTable("Question").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_QuestionFile").OnTable("QuestionFile").InSchema("dbo").OnColumn("QuestionId").Ascending()
                .WithOptions().Unique();

            #endregion

            #region SurveyResult

            Create.Table("SurveyResult").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Comment").AsString(250).Nullable()
                .WithColumn("SurveyId").AsInt32().NotNullable()
                .WithColumn("AppliedBy").AsInt32().NotNullable()
                .WithColumn("ApplyTo").AsInt32().Nullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_SurveyResult_Survey").FromTable("SurveyResult").InSchema("dbo").ForeignColumn("SurveyId")
                .ToTable("Survey").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_SurveyResult_AppliedBy").FromTable("SurveyResult").InSchema("dbo").ForeignColumn("AppliedBy")
                .ToTable("Worker").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_SurveyResult_ApplyTo").FromTable("SurveyResult").InSchema("dbo").ForeignColumn("ApplyTo")
                .ToTable("Worker").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_SurveyResult").OnTable("SurveyResult").InSchema("dbo").OnColumn("SurveyId").Ascending()
                .OnColumn("AppliedBy").Ascending().OnColumn("ApplyTo").Ascending().WithOptions().Unique();

            #endregion

            #region Answer

            Create.Table("Answer").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Result").AsString(250).NotNullable()
                .WithColumn("QuestionId").AsInt32().NotNullable()
                .WithColumn("QuestionOptionId").AsInt32().NotNullable()
                .WithColumn("SurveyResultId").AsInt32().NotNullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            Create.ForeignKey("FK_Answer_Question").FromTable("Answer").InSchema("dbo").ForeignColumn("QuestionId")
                .ToTable("Question").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_Answer_QuestionOption").FromTable("Answer").InSchema("dbo").ForeignColumn("QuestionOptionId")
                .ToTable("QuestionOption").InSchema("dbo").PrimaryColumn("Id");

            Create.ForeignKey("FK_Answer_SurveyResult").FromTable("Answer").InSchema("dbo").ForeignColumn("SurveyResultId")
                .ToTable("SurveyResult").InSchema("dbo").PrimaryColumn("Id");

            Create.Index("IX_Answer").OnTable("Answer").InSchema("dbo").OnColumn("QuestionId").Ascending()
                .OnColumn("QuestionOptionId").Ascending().OnColumn("SurveyResultId").Ascending().WithOptions().Unique();

            #endregion

            #region Planning

            Create.Table("Planning").InSchema("dbo")
                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
                .WithColumn("Recurrence").AsInt32().NotNullable()
                .WithColumn("Name").AsString(250).NotNullable()
                .WithColumn("Description").AsString(250).Nullable()
                .WithColumn("InitialDate").AsDateTimeOffset().Nullable()
                .WithColumn("ExpirationDate").AsDateTimeOffset().Nullable()
                .WithColumn("Days").AsString(250).Nullable()
                .WithColumn("CreatedBy").AsInt32().NotNullable()
                .WithColumn("ModifiedBy").AsInt32().NotNullable()
                .WithColumn("CreatedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("ModifiedOn").AsDateTimeOffset().NotNullable()
                .WithColumn("Status").AsBoolean().NotNullable()
                .WithColumn("IsActive").AsBoolean().NotNullable();

            #endregion

        }

        public override void Down()
        {

            #region Planning

            Delete.Table("Planning").InSchema("dbo");

            #endregion

            #region SurveyResult

            Delete.ForeignKey("FK_Answer_SurveyResult").OnTable("Answer").InSchema("dbo");
            Delete.ForeignKey("FK_Answer_QuestionOption").OnTable("Answer").InSchema("dbo");
            Delete.ForeignKey("FK_Answer_Question").OnTable("Answer").InSchema("dbo");
            Delete.Table("Answer").InSchema("dbo");

            #endregion

            #region SurveyResult

            Delete.ForeignKey("FK_SurveyResult_Survey").OnTable("SurveyResult").InSchema("dbo");
            Delete.ForeignKey("FK_SurveyResult_ApplyTo").OnTable("SurveyResult").InSchema("dbo");
            Delete.ForeignKey("FK_SurveyResult_AppliedBy").OnTable("SurveyResult").InSchema("dbo");
            Delete.Table("SurveyResult").InSchema("dbo");

            #endregion

            #region QuestionFile

            Delete.ForeignKey("FK_QuestionFile_Question").OnTable("QuestionFile").InSchema("dbo");
            Delete.Table("QuestionFile").InSchema("dbo");

            #endregion

            #region QuestionOption

            Delete.ForeignKey("FK_QuestionOption_Question").OnTable("QuestionOption").InSchema("dbo");
            Delete.Table("QuestionOption").InSchema("dbo");

            #endregion

            #region Question

            Delete.ForeignKey("FK_Question_Survey").OnTable("Question").InSchema("dbo");
            Delete.Table("Question").InSchema("dbo");

            #endregion

            #region Survey

            Delete.Table("Survey").InSchema("dbo");

            #endregion

            #region ProyectConfiguration

            Delete.ForeignKey("FK_ProyectConfiguration_Tag").OnTable("ProyectConfiguration").InSchema("dbo");
            Delete.ForeignKey("FK_ProyectConfiguration_Worker").OnTable("ProyectConfiguration").InSchema("dbo");
            Delete.ForeignKey("FK_ProyectConfiguration_Proyect").OnTable("ProyectConfiguration").InSchema("dbo");
            Delete.Table("ProyectConfiguration").InSchema("dbo");

            #endregion

            #region Tag

            Delete.Table("Tag").InSchema("dbo");

            #endregion

            #region History

            Delete.ForeignKey("FK_History_Proyect").OnTable("History").InSchema("dbo");
            Delete.Table("History").InSchema("dbo");

            #endregion

            #region EnviromentConfiguration

            Delete.ForeignKey("FK_EnviromentConfiguration_Enviroment").OnTable("EnviromentConfiguration").InSchema("dbo");
            Delete.Table("EnviromentConfiguration").InSchema("dbo");

            #endregion

            #region Enviroment

            Delete.ForeignKey("FK_Enviroment_Proyect").OnTable("Enviroment").InSchema("dbo");
            Delete.Table("Enviroment").InSchema("dbo");

            #endregion

            #region Proyect

            Delete.ForeignKey("FK_Proyect_BusinessUnit").OnTable("Proyect").InSchema("dbo");
            Delete.Table("Proyect").InSchema("dbo");

            #endregion

            #region SkillConfiguration

            Delete.ForeignKey("FK_SkillConfiguration_Skill").OnTable("SkillConfiguration").InSchema("dbo");
            Delete.ForeignKey("FK_SkillConfiguration_Worker").OnTable("SkillConfiguration").InSchema("dbo");
            Delete.Table("SkillConfiguration").InSchema("dbo");

            #endregion

            #region Skill

            Delete.Table("Skill").InSchema("dbo");

            #endregion

            #region Career

            Delete.ForeignKey("FK_Career_Worker").OnTable("Career").InSchema("dbo");
            Delete.Table("Career").InSchema("dbo");

            #endregion

            #region RefreshToken

            Delete.ForeignKey("FK_RefreshToken_Worker").OnTable("RefreshToken").InSchema("dbo");
            Delete.Table("RefreshToken").InSchema("dbo");

            #endregion

            #region Worker

            Delete.ForeignKey("FK_Worker_JobConfiguration").OnTable("Worker").InSchema("dbo");
            Delete.ForeignKey("FK_Worker_BusinessUnit").OnTable("Worker").InSchema("dbo");
            Delete.Table("Worker").InSchema("dbo");

            #endregion

            #region BusinessUnit

            Delete.Table("BusinessUnit").InSchema("dbo");

            #endregion

            #region JobConfiguration

            Delete.ForeignKey("FK_JobConfiguration_Level").OnTable("JobConfiguration").InSchema("dbo");
            Delete.ForeignKey("FK_JobConfiguration_Category").OnTable("JobConfiguration").InSchema("dbo");
            Delete.ForeignKey("FK_JobConfiguration_Job").OnTable("JobConfiguration").InSchema("dbo");
            Delete.Table("JobConfiguration").InSchema("dbo");

            #endregion

            #region Level

            Delete.Table("Level").InSchema("dbo");

            #endregion

            #region Category

            Delete.Table("Category").InSchema("dbo");

            #endregion

            #region Job

            Delete.Table("Job").InSchema("dbo");

            #endregion

        }
    }
}