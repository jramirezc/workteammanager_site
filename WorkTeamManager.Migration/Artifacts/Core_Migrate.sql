/* Using Database sqlserver2008 and Connection String Server=JAVIER-RAMIREZ\SQLEXPRESS; Database=WorkTeamManager; Integrated Security=true */
/* VersionMigration migrating ================================================ */

/* Beginning Transaction */
/* CreateTable VersionInfo */
CREATE TABLE [dbo].[VersionInfo] ([Version] BIGINT NOT NULL)

/* Committing Transaction */
/* VersionMigration migrated */

/* VersionUniqueMigration migrating ========================================== */

/* Beginning Transaction */
/* CreateIndex VersionInfo (Version) */
CREATE UNIQUE CLUSTERED INDEX [UC_Version] ON [dbo].[VersionInfo] ([Version] ASC)

/* AlterTable VersionInfo */
/* No SQL statement executed. */

/* CreateColumn VersionInfo AppliedOn DateTime */
ALTER TABLE [dbo].[VersionInfo] ADD [AppliedOn] DATETIME

/* Committing Transaction */
/* VersionUniqueMigration migrated */

/* VersionDescriptionMigration migrating ===================================== */

/* Beginning Transaction */
/* AlterTable VersionInfo */
/* No SQL statement executed. */

/* CreateColumn VersionInfo Description String */
ALTER TABLE [dbo].[VersionInfo] ADD [Description] NVARCHAR(1024)

/* Committing Transaction */
/* VersionDescriptionMigration migrated */

/* 1: _1_CoreMigration migrating ============================================= */

/* Beginning Transaction */
/* CreateTable Job */
CREATE TABLE [dbo].[Job] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Job] PRIMARY KEY ([Id]))

/* CreateTable Category */
CREATE TABLE [dbo].[Category] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Category] PRIMARY KEY ([Id]))

/* CreateTable Level */
CREATE TABLE [dbo].[Level] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Level] PRIMARY KEY ([Id]))

/* CreateTable JobConfiguration */
CREATE TABLE [dbo].[JobConfiguration] ([Id] INT NOT NULL, [Code] NVARCHAR(250) NOT NULL, [Salary] DECIMAL(10,2), [JobId] INT NOT NULL, [CategoryId] INT, [LevelId] INT, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_JobConfiguration] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_JobConfiguration_Job JobConfiguration(JobId) Job(Id) */
ALTER TABLE [dbo].[JobConfiguration] ADD CONSTRAINT [FK_JobConfiguration_Job] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([Id])

/* CreateForeignKey FK_JobConfiguration_Category JobConfiguration(CategoryId) Category(Id) */
ALTER TABLE [dbo].[JobConfiguration] ADD CONSTRAINT [FK_JobConfiguration_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([Id])

/* CreateForeignKey FK_JobConfiguration_Level JobConfiguration(LevelId) Level(Id) */
ALTER TABLE [dbo].[JobConfiguration] ADD CONSTRAINT [FK_JobConfiguration_Level] FOREIGN KEY ([LevelId]) REFERENCES [dbo].[Level] ([Id])

/* CreateIndex JobConfiguration (JobId, CategoryId, LevelId) */
CREATE UNIQUE INDEX [IX_JobConfiguration] ON [dbo].[JobConfiguration] ([JobId] ASC, [CategoryId] ASC, [LevelId] ASC)

/* CreateTable BusinessUnit */
CREATE TABLE [dbo].[BusinessUnit] ([Id] INT NOT NULL, [Code] NVARCHAR(50) NOT NULL, [Name] NVARCHAR(250) NOT NULL, [Address] NVARCHAR(250) NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_BusinessUnit] PRIMARY KEY ([Id]))

/* CreateTable Worker */
CREATE TABLE [dbo].[Worker] ([Id] INT NOT NULL, [IsAdmin] BIT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [Email] NVARCHAR(250) NOT NULL, [Password] NVARCHAR(250) NOT NULL, [BusinessUnitId] INT, [JobConfigurationId] INT, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Worker] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_Worker_BusinessUnit Worker(BusinessUnitId) BusinessUnit(Id) */
ALTER TABLE [dbo].[Worker] ADD CONSTRAINT [FK_Worker_BusinessUnit] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([Id])

/* CreateForeignKey FK_Worker_JobConfiguration Worker(JobConfigurationId) JobConfiguration(Id) */
ALTER TABLE [dbo].[Worker] ADD CONSTRAINT [FK_Worker_JobConfiguration] FOREIGN KEY ([JobConfigurationId]) REFERENCES [dbo].[JobConfiguration] ([Id])

/* CreateIndex Worker (BusinessUnitId, JobConfigurationId) */
CREATE UNIQUE INDEX [IX_Worker] ON [dbo].[Worker] ([BusinessUnitId] ASC, [JobConfigurationId] ASC)

/* CreateTable RefreshToken */
CREATE TABLE [dbo].[RefreshToken] ([Id] INT NOT NULL, [Ticket] NVARCHAR(250) NOT NULL, [IssuedDate] DATETIMEOFFSET NOT NULL, [ExpirationDate] DATETIMEOFFSET NOT NULL, [WorkerId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_RefreshToken] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_RefreshToken_Worker RefreshToken(WorkerId) Worker(Id) */
ALTER TABLE [dbo].[RefreshToken] ADD CONSTRAINT [FK_RefreshToken_Worker] FOREIGN KEY ([WorkerId]) REFERENCES [dbo].[Worker] ([Id])

/* CreateIndex RefreshToken (WorkerId) */
CREATE UNIQUE INDEX [IX_RefreshToken] ON [dbo].[RefreshToken] ([WorkerId] ASC)

/* CreateTable Career */
CREATE TABLE [dbo].[Career] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [WorkerId] INT NOT NULL, [DateEvent] DATETIMEOFFSET NOT NULL, CONSTRAINT [PK_Career] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_Career_Worker Career(WorkerId) Worker(Id) */
ALTER TABLE [dbo].[Career] ADD CONSTRAINT [FK_Career_Worker] FOREIGN KEY ([WorkerId]) REFERENCES [dbo].[Worker] ([Id])

/* CreateIndex Career (WorkerId) */
CREATE UNIQUE INDEX [IX_Career] ON [dbo].[Career] ([WorkerId] ASC)

/* CreateTable Skill */
CREATE TABLE [dbo].[Skill] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Skill] PRIMARY KEY ([Id]))

/* CreateTable SkillConfiguration */
CREATE TABLE [dbo].[SkillConfiguration] ([Id] INT NOT NULL, [Value] INT NOT NULL, [WorkerId] INT NOT NULL, [SkillId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_SkillConfiguration] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_SkillConfiguration_Worker SkillConfiguration(WorkerId) Worker(Id) */
ALTER TABLE [dbo].[SkillConfiguration] ADD CONSTRAINT [FK_SkillConfiguration_Worker] FOREIGN KEY ([WorkerId]) REFERENCES [dbo].[Worker] ([Id])

/* CreateForeignKey FK_SkillConfiguration_Skill SkillConfiguration(SkillId) Skill(Id) */
ALTER TABLE [dbo].[SkillConfiguration] ADD CONSTRAINT [FK_SkillConfiguration_Skill] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Skill] ([Id])

/* CreateIndex SkillConfiguration (WorkerId, SkillId) */
CREATE UNIQUE INDEX [IX_SkillConfiguration] ON [dbo].[SkillConfiguration] ([WorkerId] ASC, [SkillId] ASC)

/* CreateTable Proyect */
CREATE TABLE [dbo].[Proyect] ([Id] INT NOT NULL, [Tracing] INT NOT NULL, [Code] NVARCHAR(250), [Name] NVARCHAR(250) NOT NULL, [Customer] NVARCHAR(250) NOT NULL, [RepositoryRoot] NVARCHAR(250), [BusinessUnitId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Proyect] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_Proyect_BusinessUnit Proyect(BusinessUnitId) BusinessUnit(Id) */
ALTER TABLE [dbo].[Proyect] ADD CONSTRAINT [FK_Proyect_BusinessUnit] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([Id])

/* CreateIndex Proyect (BusinessUnitId) */
CREATE UNIQUE INDEX [IX_Proyect] ON [dbo].[Proyect] ([BusinessUnitId] ASC)

/* CreateTable Enviroment */
CREATE TABLE [dbo].[Enviroment] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [ProyectId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Enviroment] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_Enviroment_Proyect Enviroment(ProyectId) Proyect(Id) */
ALTER TABLE [dbo].[Enviroment] ADD CONSTRAINT [FK_Enviroment_Proyect] FOREIGN KEY ([ProyectId]) REFERENCES [dbo].[Proyect] ([Id])

/* CreateIndex Enviroment (ProyectId) */
CREATE UNIQUE INDEX [IX_Enviroment] ON [dbo].[Enviroment] ([ProyectId] ASC)

/* CreateTable EnviromentConfiguration */
CREATE TABLE [dbo].[EnviromentConfiguration] ([Id] INT NOT NULL, [Key] NVARCHAR(250) NOT NULL, [Value] NVARCHAR(250) NOT NULL, [EnviromentId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_EnviromentConfiguration] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_EnviromentConfiguration_Enviroment EnviromentConfiguration(EnviromentId) Enviroment(Id) */
ALTER TABLE [dbo].[EnviromentConfiguration] ADD CONSTRAINT [FK_EnviromentConfiguration_Enviroment] FOREIGN KEY ([EnviromentId]) REFERENCES [dbo].[Enviroment] ([Id])

/* CreateIndex EnviromentConfiguration (EnviromentId) */
CREATE UNIQUE INDEX [IX_EnviromentConfiguration] ON [dbo].[EnviromentConfiguration] ([EnviromentId] ASC)

/* CreateTable History */
CREATE TABLE [dbo].[History] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [ProyectId] INT NOT NULL, [DateEvent] DATETIMEOFFSET NOT NULL, CONSTRAINT [PK_History] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_History_Proyect History(ProyectId) Proyect(Id) */
ALTER TABLE [dbo].[History] ADD CONSTRAINT [FK_History_Proyect] FOREIGN KEY ([ProyectId]) REFERENCES [dbo].[Proyect] ([Id])

/* CreateIndex History (ProyectId) */
CREATE UNIQUE INDEX [IX_History] ON [dbo].[History] ([ProyectId] ASC)

/* CreateTable Tag */
CREATE TABLE [dbo].[Tag] ([Id] INT NOT NULL, [Code] NVARCHAR(250) NOT NULL, [Name] NVARCHAR(250) NOT NULL, [BackgroundColor] NVARCHAR(250) NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Tag] PRIMARY KEY ([Id]))

/* CreateTable ProyectConfiguration */
CREATE TABLE [dbo].[ProyectConfiguration] ([Id] INT NOT NULL, [ProyectId] INT NOT NULL, [WorkerId] INT NOT NULL, [TagId] INT, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_ProyectConfiguration] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_ProyectConfiguration_Proyect ProyectConfiguration(ProyectId) Proyect(Id) */
ALTER TABLE [dbo].[ProyectConfiguration] ADD CONSTRAINT [FK_ProyectConfiguration_Proyect] FOREIGN KEY ([ProyectId]) REFERENCES [dbo].[Proyect] ([Id])

/* CreateForeignKey FK_ProyectConfiguration_Worker ProyectConfiguration(WorkerId) Worker(Id) */
ALTER TABLE [dbo].[ProyectConfiguration] ADD CONSTRAINT [FK_ProyectConfiguration_Worker] FOREIGN KEY ([WorkerId]) REFERENCES [dbo].[Worker] ([Id])

/* CreateForeignKey FK_ProyectConfiguration_Tag ProyectConfiguration(TagId) Tag(Id) */
ALTER TABLE [dbo].[ProyectConfiguration] ADD CONSTRAINT [FK_ProyectConfiguration_Tag] FOREIGN KEY ([TagId]) REFERENCES [dbo].[Tag] ([Id])

/* CreateIndex ProyectConfiguration (ProyectId, WorkerId, TagId) */
CREATE UNIQUE INDEX [IX_ProyectConfiguration] ON [dbo].[ProyectConfiguration] ([ProyectId] ASC, [WorkerId] ASC, [TagId] ASC)

/* CreateTable Survey */
CREATE TABLE [dbo].[Survey] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [Description] NVARCHAR(250), [Value] DECIMAL(10,2), [Weighted] BIT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Survey] PRIMARY KEY ([Id]))

/* CreateTable Question */
CREATE TABLE [dbo].[Question] ([Id] INT NOT NULL, [QuestionType] INT NOT NULL, [Order] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [Value] DECIMAL(10,2), [Required] BIT, [LowerAnswerRequired] INT, [TopAnswerRequired] INT, [SurveyId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Question] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_Question_Survey Question(SurveyId) Survey(Id) */
ALTER TABLE [dbo].[Question] ADD CONSTRAINT [FK_Question_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([Id])

/* CreateIndex Question (SurveyId) */
CREATE UNIQUE INDEX [IX_Question] ON [dbo].[Question] ([SurveyId] ASC)

/* CreateTable QuestionOption */
CREATE TABLE [dbo].[QuestionOption] ([Id] INT NOT NULL, [Text] NVARCHAR(250) NOT NULL, [Value] DECIMAL(10,2), [QuestionId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_QuestionOption] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_QuestionOption_Question QuestionOption(QuestionId) Question(Id) */
ALTER TABLE [dbo].[QuestionOption] ADD CONSTRAINT [FK_QuestionOption_Question] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([Id])

/* CreateIndex QuestionOption (QuestionId) */
CREATE UNIQUE INDEX [IX_QuestionOption] ON [dbo].[QuestionOption] ([QuestionId] ASC)

/* CreateTable QuestionFile */
CREATE TABLE [dbo].[QuestionFile] ([Id] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [FilePath] NVARCHAR(250) NOT NULL, [QuestionId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_QuestionFile] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_QuestionFile_Question QuestionFile(QuestionId) Question(Id) */
ALTER TABLE [dbo].[QuestionFile] ADD CONSTRAINT [FK_QuestionFile_Question] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([Id])

/* CreateIndex QuestionFile (QuestionId) */
CREATE UNIQUE INDEX [IX_QuestionFile] ON [dbo].[QuestionFile] ([QuestionId] ASC)

/* CreateTable SurveyResult */
CREATE TABLE [dbo].[SurveyResult] ([Id] INT NOT NULL, [Comment] NVARCHAR(250), [SurveyId] INT NOT NULL, [AppliedBy] INT NOT NULL, [ApplyTo] INT, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_SurveyResult] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_SurveyResult_Survey SurveyResult(SurveyId) Survey(Id) */
ALTER TABLE [dbo].[SurveyResult] ADD CONSTRAINT [FK_SurveyResult_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([Id])

/* CreateForeignKey FK_SurveyResult_AppliedBy SurveyResult(AppliedBy) Worker(Id) */
ALTER TABLE [dbo].[SurveyResult] ADD CONSTRAINT [FK_SurveyResult_AppliedBy] FOREIGN KEY ([AppliedBy]) REFERENCES [dbo].[Worker] ([Id])

/* CreateForeignKey FK_SurveyResult_ApplyTo SurveyResult(ApplyTo) Worker(Id) */
ALTER TABLE [dbo].[SurveyResult] ADD CONSTRAINT [FK_SurveyResult_ApplyTo] FOREIGN KEY ([ApplyTo]) REFERENCES [dbo].[Worker] ([Id])

/* CreateIndex SurveyResult (SurveyId, AppliedBy, ApplyTo) */
CREATE UNIQUE INDEX [IX_SurveyResult] ON [dbo].[SurveyResult] ([SurveyId] ASC, [AppliedBy] ASC, [ApplyTo] ASC)

/* CreateTable Answer */
CREATE TABLE [dbo].[Answer] ([Id] INT NOT NULL, [Result] NVARCHAR(250) NOT NULL, [QuestionId] INT NOT NULL, [QuestionOptionId] INT NOT NULL, [SurveyResultId] INT NOT NULL, [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Answer] PRIMARY KEY ([Id]))

/* CreateForeignKey FK_Answer_Question Answer(QuestionId) Question(Id) */
ALTER TABLE [dbo].[Answer] ADD CONSTRAINT [FK_Answer_Question] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Question] ([Id])

/* CreateForeignKey FK_Answer_QuestionOption Answer(QuestionOptionId) QuestionOption(Id) */
ALTER TABLE [dbo].[Answer] ADD CONSTRAINT [FK_Answer_QuestionOption] FOREIGN KEY ([QuestionOptionId]) REFERENCES [dbo].[QuestionOption] ([Id])

/* CreateForeignKey FK_Answer_SurveyResult Answer(SurveyResultId) SurveyResult(Id) */
ALTER TABLE [dbo].[Answer] ADD CONSTRAINT [FK_Answer_SurveyResult] FOREIGN KEY ([SurveyResultId]) REFERENCES [dbo].[SurveyResult] ([Id])

/* CreateIndex Answer (QuestionId, QuestionOptionId, SurveyResultId) */
CREATE UNIQUE INDEX [IX_Answer] ON [dbo].[Answer] ([QuestionId] ASC, [QuestionOptionId] ASC, [SurveyResultId] ASC)

/* CreateTable Planning */
CREATE TABLE [dbo].[Planning] ([Id] INT NOT NULL, [Recurrence] INT NOT NULL, [Name] NVARCHAR(250) NOT NULL, [Description] NVARCHAR(250), [InitialDate] DATETIMEOFFSET, [ExpirationDate] DATETIMEOFFSET, [Days] NVARCHAR(250), [CreatedBy] INT NOT NULL, [ModifiedBy] INT NOT NULL, [CreatedOn] DATETIMEOFFSET NOT NULL, [ModifiedOn] DATETIMEOFFSET NOT NULL, [Status] BIT NOT NULL, [IsActive] BIT NOT NULL, CONSTRAINT [PK_Planning] PRIMARY KEY ([Id]))

INSERT INTO [dbo].[VersionInfo] ([Version], [AppliedOn], [Description]) VALUES (1, '2017-06-21T20:26:15', '_1_CoreMigration')
/* Committing Transaction */
/* 1: _1_CoreMigration migrated */

/* Task completed. */
