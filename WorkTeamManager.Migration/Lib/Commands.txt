﻿**Run Migration
cd 'D:\Proyects\Binarium\WorkTeamManager\Site\WorkTeamManager.Migration\Build'
msbuild MigrationInitial.msbuild -t:Migrate

**Connection String
Server=JAVIER-RAMIREZ\SQLEXPRESS; Database=WorkTeamManager; Integrated Security=true

**Isolation Level
ALTER DATABASE INSJPY SET ALLOW_SNAPSHOT_ISOLATION ON

**Configure var
$name = $env:Path
$newName = $name + ';C:\Windows\Microsoft.NET\Framework\v4.0.30319'
[Environment]::SetEnvironmentVariable("Path", $newName, "User")
$env:Path